package ictgradschool.web.lab14.ex03;




import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.stream.events.Namespace;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class ImageGalleryDisplay extends HttpServlet {


    public ImageGalleryDisplay() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO Auto-generated method stub
        //response.getWriter().append("Served at: ").append(request.getContextPath());

        //displayBasic(request, response);


        displayHTML(request, response);
    }

    public void displayHTML(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        ServletContext servletContext = getServletContext();
        String fullPhotoPath = servletContext.getRealPath("/Photos");
        PrintWriter out = response.getWriter();

        out.println("<html><title>Image Gallery</title>");
        out.println("<h1>Check out these animals, yo!</h1>");

        File path = new File(fullPhotoPath);

        File[] files = path.listFiles();

        if (files == null) {
            out.println("<p>File is empty</p>");
        }
        List<String> images = new ArrayList<>();
        List<String> thumbnails = new ArrayList<>();

        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile()) {

                String name = files[i].getName();
                if (name.split("thumbnail").length == 2) {
                    thumbnails.add(name);
                }
                    else if (name.substring(name.length() - 3).equals("jpg") || name.substring(name.length() - 3).equals("png")){ images.add(name); }
                }
            }

            for (int j = 0; j < images.size(); j++){
            String wosuffix = images.get(j).split(".jpg")[0];
            String[] underscore = wosuffix.split("_");
            String name = "";
            for (String s: underscore){
                name += s + " ";
            }
            out.println("<h3>" + name + "</h3>" + "<a href='/Photos/" + images.get(j) + "'>");
                out.println("<img src='/Photos/" + thumbnails.get(j) + "'></a>");
            }

        out.println("</body></html>");

    }
}

