package ictgradschool.web.lab14.ex04;


import org.apache.commons.fileupload.FileItem;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;


import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.tools.ForwardingFileObject;

import java.awt.*;
import java.awt.image.BufferedImage;

import java.awt.image.RenderedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;

import java.io.Serializable;
import java.util.Iterator;
import java.util.List;


public class UploadServlet extends HttpServlet {


    private boolean isMultipart;
    private String filePath;
//    private int maxFileSize = 50 * 1024;
//    private int maxMemSize = 4 * 1024;
    private File file;

    public void init() {

        // Get the file location where it would be stored.
        filePath = getServletContext().getRealPath("uploads");
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, java.io.IOException {

        isMultipart = ServletFileUpload.isMultipartContent(request);
        response.setContentType("text/html");
        java.io.PrintWriter out = response.getWriter();

        if (!isMultipart) {
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<p>No file uploaded</p>");
            out.println("</body>");
            out.println("</html>");
            return;
        }

        new File(filePath).mkdir();

        DiskFileItemFactory factory = new DiskFileItemFactory();

        // maximum size that will be stored in memory
//        factory.setSizeThreshold(maxMemSize);

        // Location to save data that is larger than maxMemSize.
        factory.setRepository(new File("c:\\temp"));

        // Create a new file upload handler
        ServletFileUpload upload = new ServletFileUpload(factory);

        // maximum file size to be uploaded.
//        upload.setSizeMax(maxFileSize);

        String caption = "";

        String fileName = "";

        try {
            // Parse the request to get file items.
            List fileItems = upload.parseRequest(request);
            // Process the uploaded file items
            Iterator i = fileItems.iterator();

            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet upload</title>");
            out.println("</head>");
            out.println("<body>");
            String appendFileName = "";

            while (i.hasNext()) {
                Object next = i.next();
                FileItem fi = (FileItem) next;



                if (!fi.isFormField()) {

                    fileName = fi.getName();


                   BufferedImage img;

                    // Write the file
                    if (fileName.lastIndexOf("\\") >= 0) {
                        file = new File(filePath + File.separator + fileName.substring(fileName.lastIndexOf("\\")));
                        appendFileName = fileName.substring(fileName.lastIndexOf("\\")).split("\\.")[0] + "_thumb.png";

                    } else {
                        file = new File(filePath + File.separator + fileName.substring(fileName.lastIndexOf("\\") + 1));
                        appendFileName = fileName.substring(fileName.lastIndexOf("\\") + 1).split("\\.")[0] + "_thumb.png";
                    }
                    fi.write(file);

                    File thumbnail = new File(filePath + File.separator + appendFileName);

                    BufferedImage image1 = ImageIO.read(file);
                    double height= image1.getHeight(null);
                    double width = image1.getWidth(null);
                    double aspectRatio = height/width;


                    if(aspectRatio >=1) {
                        img = new BufferedImage((int) (100/aspectRatio), 100, BufferedImage.TYPE_INT_RGB);//create image of certain size
                        //This next line reads our file, resizes the image in the file and saves as the buffered image from before
                        img.createGraphics().drawImage(ImageIO.read(file).getScaledInstance(100, 100, Image.SCALE_SMOOTH), 0, 0, null);
                    }

                    else {
                        img = new BufferedImage(100, (int) (100*aspectRatio),  BufferedImage.TYPE_INT_RGB);//create image of certain size
                        //This next line reads our file, resizes the image in the file and saves as the buffered image from before
                        img.createGraphics().drawImage(ImageIO.read(file).getScaledInstance(100, (int) (100*aspectRatio), Image.SCALE_SMOOTH), 0, 0, null);

                    }
                    ImageIO.write(img, "jpg", thumbnail);



                } else {
                    caption = fi.getString();
                }

            }
            out.println("<a href='/uploads" + File.separator + fileName + "'><img src='/uploads" + File.separator + appendFileName + "'>" + caption + "</a>");
            out.println("</body>");
            out.println("</html>");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, java.io.IOException {

        throw new ServletException("GET method used with " +
                getClass().getName() + ": POST method required.");
    }

    private void displayBasic(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    private void displayHTML(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


    }
}
